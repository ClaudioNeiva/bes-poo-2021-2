package br.ucsal.bes.poo20212.aula03.escola.domain;

// Exemplo de como era feito, mas NÃO DEVE mais ser feito.
public class Escola {

	public static void main(String[] args) {
		String nomesAluno[];
		String emailsAluno[];

		String nomesProfessor[];
		Double notas[][];
		Boolean frequencia[][];
		String nomesDisciplina[];
		String codigosDisciplina[];
	}

	static void matricularAluno() {
		// aqui escrever o código para permitir a matrícula dos alunos
	}

	static void contratarProfessor() {
		// aqui escrever o código para permitir a contratação dos professores
	}

	static void registrarFrequencia() {
		// aqui escrever o código para permitir o registros da frequência
	}

}
