package br.ucsal.bes.poo20212.aula03.escola.domain;

public class Exemplo {

	private static final int QTD_MAX_ALUNOS = 400000000;

	public static void main(String[] args) {
		Aluno aluno1 = new Aluno();
		Aluno aluno2 = new Aluno();
		aluno1.matricula = 123123;
		aluno1.nome = "Lucas";
		aluno1.telefone = "7193495438";
		aluno1.email = "lucas@aluno.ucsal.br";
		System.out.println(aluno1);
		System.out.println(aluno2);
		System.out.println("Nome do aluno1: " + aluno1.nome);

		Aluno[] alunos = new Aluno[QTD_MAX_ALUNOS];
		for (int i = 0; i < QTD_MAX_ALUNOS; i++) {
			alunos[i] = new Aluno();
		}

	}

}
