package br.ucsal.bes.poo20212.aula07.atribclassstatic;

public class Exemplo2 {

	public static void main(String[] args) {

		Aluno.setContador(100);

		Aluno alunoA = new Aluno("antonio", "antonio@ucsal.br"); // alunoA.matricula = 1
		Aluno alunoB = new Aluno("claudio", "claudio@ucsal.br"); // alunoB.matricula = 2
		Aluno alunoC = new Aluno("neiva", "neiva@ucsal.br"); // alunoC.matricula = 3

		System.out.println("alunoA=" + alunoA);
		System.out.println("alunoB=" + alunoB);
		System.out.println("alunoC=" + alunoC);

		System.out.println("Aluno.getContador()=" + Aluno.getContador());

	}

}
