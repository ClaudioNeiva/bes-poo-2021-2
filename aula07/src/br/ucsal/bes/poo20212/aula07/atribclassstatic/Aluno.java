package br.ucsal.bes.poo20212.aula07.atribclassstatic;

public class Aluno {

	private static Integer contador = 0;

	private Integer matricula;

	private String nome;

	private String email;

	// O valor da matrícula seja criado durante a instanciação como um sequência de
	// números.
	public Aluno(String nome, String email) {
		definirMatricula();
		this.nome = nome;
		this.email = email;
	}

	private void definirMatricula() {
		// Um método de instância PODE manipular um atributo estático:
		contador++;
		this.matricula = contador;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public static Integer getContador() {
		return contador;
	}
	
	public static void setContador(Integer contador) {
		Aluno.contador = contador;
		// Um método estático NÃO pode manipular um atributo de instância:
		// nome="claudio";
	}
	
	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", email=" + email + "]";
	}

}
