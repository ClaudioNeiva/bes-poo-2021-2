package br.ucsal.bes.poo20212.aula07.listas;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes.poo20212.aula07.atribclassstatic.Aluno;

public class ExemploInterfaceList {

	public static void main(String[] args) {

		// lista de nomes

		// Serviços para o tratamento de uma lista?
		// add(content)
		// remove(index)
		// get(index)
		// size()

		// Implementações possíveis para criar uma lista?
		// Baseada em array <-- ver em estrutura de dados
		// Baseada em listas ligadas <-- ver em estrutura de dados

		// List<String> nomes = new LinkedList<>();
		List<String> nomes = new ArrayList<>();

		nomes.add("antonio");
		nomes.add("claudio");
		nomes.add("neiva");

		System.out.println("tamanho da lista de nomes: " + nomes.size());

		System.out.println("o segundo elemento da lista é: " + nomes.get(1));

		nomes.remove(1);

		System.out.println("nomes na lista: " + nomes);

		List<Integer> numeros = new ArrayList<>();
		numeros.add(10);

		int a = 20;
		numeros.add(a);

	}

}
