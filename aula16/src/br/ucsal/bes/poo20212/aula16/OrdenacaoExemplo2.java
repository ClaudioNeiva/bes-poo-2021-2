package br.ucsal.bes.poo20212.aula16;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoExemplo2 {

	public static void main(String[] args) {
		ordenacaoJava8();
	}

	private static void ordenacaoJava8() {

		List<Aluno> alunos = new ArrayList<>();

		alunos.add(new Aluno(200, "123", "claudio", LocalDate.of(1975,5,1)));
		alunos.add(new Aluno(180, "345", "antonio", LocalDate.of(2000,6,20)));
		alunos.add(new Aluno(190, "321", "pedreira", LocalDate.of(2000,6,20)));
		alunos.add(new Aluno(175, "154", "neiva", LocalDate.of(1990,7,5)));

		System.out.println("Alunos na ordem em que foram informados:");
		alunos.forEach(System.out::println);
		
		System.out.println("\nNomes na ordem em \"decrescente de ano de nascimento\":");
		alunos.sort(Comparator.reverseOrder());
		alunos.forEach(System.out::println);

		System.out.println("\nAlunos em ordem \"crescente de ano de nascimento\":");
		alunos.sort(Comparator.naturalOrder());
		alunos.forEach(System.out::println);

		System.out.println("\nNomes na ordem em \"decrescente de matrícula\":");
		// alunos.sort(Comparator.reverseOrder()); ????
		alunos.forEach(System.out::println);

		System.out.println("\nAlunos em ordem \"crescente de matrícula\":");
		// alunos.sort(Comparator.naturalOrder()); ????
		alunos.forEach(System.out::println);

	}

}
