package br.ucsal.bes.poo20212.aula16;

import java.time.LocalDate;

public class Aluno implements Comparable<Aluno> {

	private Integer matricula;

	private String cpf;

	private String nome;

	private LocalDate dataNascimento;

	public Aluno(Integer matricula, String cpf, String nome, LocalDate dataNascimento) {
		super();
		this.matricula = matricula;
		this.cpf = cpf;
		this.nome = nome;
		this.dataNascimento = dataNascimento;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", cpf=" + cpf + ", nome=" + nome + ", dataNascimento="
				+ dataNascimento + "]";
	}

	@Override
	// negativo se o this menor que o other
	// zero se o this igual ao other
	// positivo se o this amaior que other
	public int compareTo(Aluno other) {
		return dataNascimento.compareTo(other.dataNascimento);
	}

}
