package br.ucsal.bes.poo20212.aula16;

import java.util.ArrayList;
import java.util.List;

public class FormasIteracaoListExemplo {

	public static void main(String[] args) {

		List<String> nomes = new ArrayList<>();

		nomes.add("claudio");
		nomes.add("antonio");
		nomes.add("pedreira");
		nomes.add("neiva");

		System.out.println("Nomes na ordem em que foram informados:");
		nomes.forEach(System.out::println);

		System.out.println("\nNomes na ordem em que foram informados:");
		nomes.forEach(nome -> System.out.println(nome));

		System.out.println("\nNomes na ordem em que foram informados:");
		nomes.forEach(nome -> {
			System.out.println(nome);
		});

		System.out.println("\nNomes na ordem em que foram informados:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println("\nNomes na ordem em que foram informados:");
		for (int i = 0; i < nomes.size(); i++) {
			String nome = nomes.get(i);
			System.out.println(nome);
		}

		System.out.println("\nNomes na ordem em que foram informados:");
		System.out.println(nomes);

	}

}
