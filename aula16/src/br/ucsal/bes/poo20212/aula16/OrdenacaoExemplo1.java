package br.ucsal.bes.poo20212.aula16;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoExemplo1 {

	public static void main(String[] args) {
		ordenacaoJava8();
	}

	private static void ordenacaoJava8() {

		List<String> nomes = new ArrayList<>();

		nomes.add("claudio");
		nomes.add("antonio");
		nomes.add("pedreira");
		nomes.add("neiva");

		System.out.println("Nomes na ordem em que foram informados:");
		nomes.forEach(System.out::println);
		
		System.out.println("\nNomes em ordem \"afabética decrescente\":");
		nomes.sort(Comparator.reverseOrder());
		nomes.forEach(System.out::println);

		System.out.println("\nNomes em ordem \"afabética crescente\":");
		nomes.sort(Comparator.naturalOrder());
		nomes.forEach(System.out::println);
	}

}
