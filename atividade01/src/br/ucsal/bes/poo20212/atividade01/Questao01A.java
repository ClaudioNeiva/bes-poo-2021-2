package br.ucsal.bes.poo20212.atividade01;

import java.util.Scanner;

public class Questao01A {

	public static void main(String[] args) {
		
		obterNotaCalcularExibirConceito();
		
	}

	private static void obterNotaCalcularExibirConceito() {
		// Obter um número de 0 a 100 - ENTRADA DE DADOS.
		int n;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe um número (de 0 a 100):");
		n = scanner.nextInt();

		// Calcular o conceito - PROCESSAMENTO.
		String conceito;
		if (n <= 49) {
			conceito = "insuficiente";
		} else if (n <= 64) {
			conceito = "regular";
		} else if (n <= 84) {
			conceito = "bom";
		} else {
			conceito = "ótimo";
		}

		// Exibir o conceito - SAÍDA.
		System.out.println("O conceito do aluno é " + conceito + ".");
	}

}
