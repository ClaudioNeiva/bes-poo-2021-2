package br.ucsal.bes.poo20212.atividade01;

import java.util.Scanner;

public class Questao01C {

	public static void main(String[] args) {
		obterNotaCalcularExibirConceito();
	}

	private static void obterNotaCalcularExibirConceito() {
		int n = obterNumeroFaixa();
		String conceito = calcularConceito(n);
		exibirConceito(conceito);
	}

	private static void exibirConceito(String conceito) {
		System.out.println("O conceito do alune é " + conceito + ".");
	}

	private static String calcularConceito(int n) {
		String conceito;
		if (n <= 49) {
			conceito = "insuficiente";
		} else if (n <= 64) {
			conceito = "regular";
		} else if (n <= 84) {
			conceito = "bom";
		} else {
			conceito = "ótimo";
		}
		return conceito;
	}

	private static int obterNumeroFaixa() {
		int n;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe um número (de 0 a 100):");
		n = scanner.nextInt();
		return n;
	}

}
