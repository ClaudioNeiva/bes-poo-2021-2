package br.ucsal.bes.poo20212.atividade01;

import java.util.Scanner;

/**
 * Crie um programa em Java que lê um valor N, inteiro e positivo, calcula e
 * escreve o valor de E (soma dos inversos dos fatoriais de 0 a N):
 * 
 * E = 1 + 1 / 1! + 1 / 2! + 1 / 3! + ... + 1 / N!
 */
public class Questao02 {

	public static void main(String[] args) {
		obterNCalcularExibirE();
	}

	private static void obterNCalcularExibirE() {
		int n = obterNInteiroPositivo();
		double e = calcularE(n);
		exibirE(n, e);
	}

	private static void exibirE(int n, double e) {
		System.out.println("E(" + n + ")=" + e);
	}

	private static double calcularE(int n) {
		double e = 0;
		for (int i = 0; i <= n; i++) {
			long fatorial = calcularFatorial(i);
			e += 1d / fatorial;
		}
		return e;
	}

	private static long calcularFatorial(int n) {
		long fatorial = 1;
		for (int i = 1; i <= n; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

	private static int obterNInteiroPositivo() {
		Scanner scanner = new Scanner(System.in);
		int n;
		while (true) {
			System.out.println("Informe um número inteiro e positivo:");
			n = scanner.nextInt();
			if (n >= 0) {
				return n;
			} else {
				System.out.println("Número fora da faixa.");
			}
		}
	}

	private static int obterNInteiroPositivo2() {
		Scanner scanner = new Scanner(System.in);
		int n;
		do {
			System.out.println("Informe um número inteiro e positivo:");
			n = scanner.nextInt();
			if (n < 0) {
				System.out.println("Número fora da faixa.");
			}
		} while (n < 0);
		return n;
	}

	private static int obterNInteiroPositivo3() {
		Scanner dado = new Scanner(System.in);
		int n;
		do {
			System.out.println("Escreva um valor inteiro e positivo: ");
			n = dado.nextInt();
			if (n < 0) {
				System.out.println("Número fora do intervalo solicitado");
			} else {
				return n;
			}
		} while (n < 0); // n < 0 é sempre TRUE
		return n;
	}
}
