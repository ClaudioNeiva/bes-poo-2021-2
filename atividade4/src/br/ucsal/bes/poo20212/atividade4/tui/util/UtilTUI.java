package br.ucsal.bes.poo20212.atividade4.tui.util;

import java.util.InputMismatchException;
import java.util.Scanner;

// FIXME Criar um construtor privado quando explicar o que são métodos de classe e métodos de instância. 
public class UtilTUI {

	private static final Scanner scanner = new Scanner(System.in);

	public static double obterDouble(String mensagem) {
		System.out.println(mensagem);
		double numero = scanner.nextDouble();
		scanner.nextLine();
		return numero;
	}

	public static Integer obterInteger(String mensagem) {
		System.out.println(mensagem);
		try {
			int numero = scanner.nextInt();
			return numero;
		} finally {
			scanner.nextLine();
		}
	}

	public static String obterString(String mensagem) {
		System.out.println(mensagem);
		return scanner.nextLine();
	}

}
