package br.ucsal.bes.poo20212.atividade4;

import br.ucsal.bes.poo20212.atividade4.gui.LocadoraGUI;
import br.ucsal.bes.poo20212.atividade4.tui.MenuTUI;

public class Main {

	public static void main(String[] args) {
		if (args.length > 0 && args[0].equalsIgnoreCase("tui")) {
			MenuTUI.executar();
		} else {
			LocadoraGUI.inicializar();
		}
	}

}
