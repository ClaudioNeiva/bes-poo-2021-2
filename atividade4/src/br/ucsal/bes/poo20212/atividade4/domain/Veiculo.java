package br.ucsal.bes.poo20212.atividade4.domain;

import java.io.Serializable;
import java.time.LocalDate;

//FIXME Os atributos foram definidos PROVISORIAMENTE como públicos enquanto não conversamos sobre ENCAPSULAMENTO.
public class Veiculo implements Serializable {

	private static final long serialVersionUID = 2L;

	private String placa;

	private Integer anoFabricacao;

	private Double valor;

	private Pessoa proprietario;
	
	private LocalDate dataCompra;

	public Veiculo() {
	}

	public Veiculo(String placa) {
		this.placa = placa.toUpperCase();
	}

	public Veiculo(String placa, Double valor) {
		this(placa);
		setValor(valor);
	}

	public Veiculo(String placa, Integer anoFabricacao, Double valor) {
		this(placa, valor);
		this.anoFabricacao = anoFabricacao;
	}

	public void setValor(Double valor) {
		if (valor > 0) {
			this.valor = valor;
		} else {
			this.valor = 0d;
		}
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public Pessoa getProprietario() {
		return proprietario;
	}

	public void setProprietario(Pessoa proprietario) {
		this.proprietario = proprietario;
	}

	public Double getValor() {
		return valor;
	}

	public LocalDate getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(LocalDate dataCompra) {
		this.dataCompra = dataCompra;
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", anoFabricacao=" + anoFabricacao + ", valor=" + valor + ", proprietario="
				+ proprietario + ", dataCompra=" + dataCompra + "]";
	}

}
