package br.ucsal.bes.poo20212.atividade4.domain;

import java.io.Serializable;

// ALT + SHIFT + R = renomar qualquer coisa, como uma classe, atributo, variável local, método, etc.
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final int QTD_MAX_TELEFONES = 10;

	private String cpf;

	private String nome;

	private Endereco endereco;

	private String[] telefones = new String[QTD_MAX_TELEFONES];

	private int qtdTelefones = 0;

	public void addTelefone(String telefone) {
		telefones[qtdTelefones] = telefone;
		qtdTelefones++;
	}

	public String getCpf() {
		return cpf;
	}

	public String getNome() {
		return nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public String[] getTelefones() {
		return telefones;
	}

}
