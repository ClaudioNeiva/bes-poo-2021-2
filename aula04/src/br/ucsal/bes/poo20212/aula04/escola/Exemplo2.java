package br.ucsal.bes.poo20212.aula04.escola;

public class Exemplo2 {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno();
		Aluno aluno2 = aluno1;

		aluno1.nome = "Claudio";
		aluno2.nome = "Maria";

		System.out.println("aluno1.nome=" + aluno1.nome);
		System.out.println("aluno2.nome=" + aluno2.nome);

	}

}
