package br.ucsal.bes.poo20212.aula04.escola;

public class Exemplo {

	private static final int QTD_MAX_ALUNOS = 40000;

	public static void main(String[] args) {

		Aluno[] alunos = new Aluno[QTD_MAX_ALUNOS];

		alunos[0] = new Aluno();
		alunos[0].nome = "Claudio";
		System.out.println(alunos[0].nome);

		for (int i = 0; i < QTD_MAX_ALUNOS; i++) {
			alunos[i] = new Aluno();
		}

	}

}
