package br.ucsal.bes.poo20212.aula04.escola;

public class Exemplo4 {

	private static final int QTD_ALUNOS_ARRAY = 10;

	public static void main(String[] args) {

		Aluno[] alunos = new Aluno[QTD_ALUNOS_ARRAY];

		System.out.println(alunos);

		alunos[3] = new Aluno();
		
		alunos[3].matricula= 3455;
		alunos[3].nome= "Claudio";
		alunos[3].email= "claudio@ucsal.br";
		alunos[3].telefone= "7167348675438";

		alunos[5].matricula= 7342;
		
		for (int i = 0; i < QTD_ALUNOS_ARRAY; i++) {
			System.out.println("alunos[" + i + "]=" + alunos[i]);
		}

		System.out.println("alunos[3].matricula="+alunos[3].matricula);
		System.out.println("alunos[3].nome="+alunos[3].nome);
		System.out.println("alunos[3].email="+alunos[3].email);
		System.out.println("alunos[3].telefone="+alunos[3].telefone);
		
	}

}
