package br.ucsal.bes.poo20212.aula04.escola;

public class Exemplo5 {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno();
		Aluno aluno2 = new Aluno();

		aluno1.matricula = 3455;
		aluno1.nome = "Claudio";
		aluno1.email = "claudio@ucsal.br";
		aluno1.telefone = "7167348675438";

		aluno2.matricula = 3455;
		aluno2.nome = "Claudio";
		aluno2.email = "claudio@ucsal.br";
		aluno2.telefone = "7167348675438";

		if (aluno1 == aluno2) {
			System.out.println("aluno1 aponta para a mesma instância apontada pelo aluno2");
		} else {
			System.out.println("aluno1 aponta para uma instância diferente da apontada pelo aluno2");
		}

		System.out.println("aluno1=" + aluno1);
		System.out.println("aluno2=" + aluno2);

	}

}
