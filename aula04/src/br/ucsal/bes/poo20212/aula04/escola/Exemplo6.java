package br.ucsal.bes.poo20212.aula04.escola;

public class Exemplo6 {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno();
		Aluno aluno2 = new Aluno();

		aluno1.matricula = 3455;
		aluno1.nome = "Claudio";
		aluno1.email = "claudio@ucsal.br";
		aluno1.telefone = "7167348675438";

		aluno2.matricula = 3455;
		aluno2.nome = "Claudio";
		aluno2.email = "claudio@ucsal.br";
		aluno2.telefone = "7167348675438";

		aluno1.matricular();
		
		if (aluno1.equals(aluno2)) {
			System.out.println("aluno1 é igual ao aluno2");
		} else {
			System.out.println("aluno1 é diferente do aluno2");
		}

		System.out.println("aluno1=" + aluno1);
		System.out.println("aluno2=" + aluno2);
		
		System.out.println("aluno1.matricula="+aluno1.matricula);
		System.out.println("aluno1.nome="+aluno1.nome);
		System.out.println("aluno1.email="+aluno1.email);
		System.out.println("aluno1.telefone="+aluno1.telefone);
		
		System.out.println("aluno2.matricula="+aluno2.matricula);
		System.out.println("aluno2.nome="+aluno2.nome);
		System.out.println("aluno2.email="+aluno2.email);
		System.out.println("aluno2.telefone="+aluno2.telefone);
		

	}

}

