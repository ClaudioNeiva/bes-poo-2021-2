package br.ucsal.bes.poo20212.tuitools;

public interface ComponenteTui {

	void apresentar();
	
	void outroMetodo();
	
}
