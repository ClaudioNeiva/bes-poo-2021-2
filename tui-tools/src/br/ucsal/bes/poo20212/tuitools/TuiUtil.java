package br.ucsal.bes.poo20212.tuitools;

public class TuiUtil {

	public static void apresentarJanela(String titulo, ComponenteTui componenteTui) {
		System.out.println("#########################################################");
		System.out.println("################### "+titulo+" ###################");

		componenteTui.apresentar();
		componenteTui.outroMetodo();
		
		System.out.println("#########################################################");
	}
	
}
