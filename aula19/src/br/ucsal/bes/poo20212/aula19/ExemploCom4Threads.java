package br.ucsal.bes.poo20212.aula19;

public class ExemploCom4Threads {

	// Duração = 13 segundos.
	// Duração = 13 segundos.

	public static void main(String[] args) throws InterruptedException {
		exibirPrimos(100000, 200000);
	}

	private static void exibirPrimos(int min, int max) throws InterruptedException {
		Long inicio = System.currentTimeMillis();

		int inc = (max - min) / 4;
		CalculoRunnable calculoRunnable1 = new CalculoRunnable(min, min + inc);
		min += inc + 1;
		CalculoRunnable calculoRunnable2 = new CalculoRunnable(min, min + inc);
		min += inc + 1;
		CalculoRunnable calculoRunnable3 = new CalculoRunnable(min, min + inc);
		min += inc + 1;
		CalculoRunnable calculoRunnable4 = new CalculoRunnable(min, max);

		Thread thread1 = new Thread(calculoRunnable1);
		Thread thread2 = new Thread(calculoRunnable2);
		Thread thread3 = new Thread(calculoRunnable3);
		Thread thread4 = new Thread(calculoRunnable4);

		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();

		thread1.join();
		thread2.join();
		thread3.join();
		thread4.join();

		Long fim = System.currentTimeMillis();
		Long duracao = fim - inicio;
		System.out.println("Duração = " + duracao / 1000 + " segundos.");
	}

}
