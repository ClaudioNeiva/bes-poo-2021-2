package br.ucsal.bes.poo20212.aula19;

public class CalculoRunnable implements Runnable {

	private int min;

	private int max;

	public CalculoRunnable(int min, int max) {
		this.min = min;
		this.max = max;
		System.out.println(this);
	}

	@Override
	public void run() {
		for (int i = min; i <= max; i++) {
			Calculo.exibirPrimo(i);
		}
	}

	@Override
	public String toString() {
		return "CalculoRunnable [min=" + min + ", max=" + max + "]";
	}

}
