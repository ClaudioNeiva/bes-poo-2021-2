package br.ucsal.bes.poo20212.aula19;

public class ExemploSemThread {

	// Duração = 36 segundos.
	// Duração = 36 segundos.

	public static void main(String[] args) {
		exibirPrimos(100000, 200000);
	}

	private static void exibirPrimos(int min, int max) {
		Long inicio = System.currentTimeMillis();
		for (int i = min; i <= max; i++) {
			Calculo.exibirPrimo(i);
		}
		Long fim = System.currentTimeMillis();
		Long duracao = fim - inicio;
		System.out.println("Duração = " + duracao / 1000 + " segundos.");
	}

}
