package br.ucsal.bes.poo20212.aula19;

public class Calculo {

	private Calculo() {
	}

	public static void exibirPrimo(int n) {
		if (qtdDivisores(n) == 2) {
			System.out.println(n);
		}
	}

	public static int qtdDivisores(int n) {
		int qtd = 0;
		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				qtd++;
			}
		}
		return qtd;
	}

}
