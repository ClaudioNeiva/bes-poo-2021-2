package br.ucsal.bes.poo20212.aula19;

public class ExemploCom2Threads {

	// Duração = 21 segundos.
	// Duração = 21 segundos.

	public static void main(String[] args) throws InterruptedException {
		exibirPrimos(100000, 200000);
	}

	private static void exibirPrimos(int min, int max) throws InterruptedException {
		Long inicio = System.currentTimeMillis();

		int meio = min + (max - min) / 2;
		CalculoRunnable calculoRunnable1 = new CalculoRunnable(min, meio);
		CalculoRunnable calculoRunnable2 = new CalculoRunnable(meio + 1, max);

		Thread thread1 = new Thread(calculoRunnable1);
		Thread thread2 = new Thread(calculoRunnable2);

		thread1.start();
		thread2.start();
		
		thread1.join();
		thread2.join();

		Long fim = System.currentTimeMillis();
		Long duracao = fim - inicio;
		System.out.println("Duração = " + duracao / 1000 + " segundos.");
	}

}
