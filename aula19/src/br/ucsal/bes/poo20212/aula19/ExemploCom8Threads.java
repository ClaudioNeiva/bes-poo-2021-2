package br.ucsal.bes.poo20212.aula19;

public class ExemploCom8Threads {

	// Duração = 11 segundos.
	// Duração = 12 segundos.

	public static void main(String[] args) throws InterruptedException {
		exibirPrimos(100000, 200000);
	}

	private static void exibirPrimos(int min, int max) throws InterruptedException {
		Long inicio = System.currentTimeMillis();

		int inc = (max - min) / 8;
		CalculoRunnable calculoRunnable1 = new CalculoRunnable(min, min + inc);
		min += inc + 1;
		CalculoRunnable calculoRunnable2 = new CalculoRunnable(min, min + inc);
		min += inc + 1;
		CalculoRunnable calculoRunnable3 = new CalculoRunnable(min, min + inc);
		min += inc + 1;
		CalculoRunnable calculoRunnable4 = new CalculoRunnable(min, min + inc);
		min += inc + 1;
		CalculoRunnable calculoRunnable5 = new CalculoRunnable(min, min + inc);
		min += inc + 1;
		CalculoRunnable calculoRunnable6 = new CalculoRunnable(min, min + inc);
		min += inc + 1;
		CalculoRunnable calculoRunnable7 = new CalculoRunnable(min, min + inc);
		min += inc + 1;
		CalculoRunnable calculoRunnable8 = new CalculoRunnable(min, max);

		Thread thread1 = new Thread(calculoRunnable1);
		Thread thread2 = new Thread(calculoRunnable2);
		Thread thread3 = new Thread(calculoRunnable3);
		Thread thread4 = new Thread(calculoRunnable4);
		Thread thread5 = new Thread(calculoRunnable5);
		Thread thread6 = new Thread(calculoRunnable6);
		Thread thread7 = new Thread(calculoRunnable7);
		Thread thread8 = new Thread(calculoRunnable8);

		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
		thread6.start();
		thread7.start();
		thread8.start();

		thread1.join();
		thread2.join();
		thread3.join();
		thread4.join();
		thread5.join();
		thread6.join();
		thread7.join();
		thread8.join();

		Long fim = System.currentTimeMillis();
		Long duracao = fim - inicio;
		System.out.println("Duração = " + duracao / 1000 + " segundos.");
	}

}
