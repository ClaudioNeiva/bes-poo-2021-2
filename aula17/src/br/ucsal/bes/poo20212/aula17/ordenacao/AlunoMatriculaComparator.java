package br.ucsal.bes.poo20212.aula17.ordenacao;

import java.util.Comparator;

// Geralmente a ordenação NÃO cria essa classe!
public class AlunoMatriculaComparator implements Comparator<Aluno> {

	@Override
	public int compare(Aluno arg0, Aluno arg1) {
		return arg0.getMatricula().compareTo(arg1.getMatricula());
	}

}
