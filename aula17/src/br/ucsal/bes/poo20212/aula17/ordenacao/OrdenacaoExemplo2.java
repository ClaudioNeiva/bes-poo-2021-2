package br.ucsal.bes.poo20212.aula17.ordenacao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoExemplo2 {

	public static void main(String[] args) {
		ordenacaoJava8();
		ordenacaoJava7();
	}

	private static void ordenacaoJava8() {

		System.out.println("######## ordenacaoJava8 ########\n");

		List<Aluno> alunos = new ArrayList<>();

		alunos.add(new Aluno(200, "123", "claudio", LocalDate.of(1975, 5, 1)));
		alunos.add(new Aluno(180, "345", "antonio", LocalDate.of(2000, 6, 20)));
		alunos.add(new Aluno(190, "321", "pedreira", LocalDate.of(2000, 6, 20)));
		alunos.add(new Aluno(175, "154", "neiva", LocalDate.of(1990, 7, 5)));

		System.out.println("Alunos na ordem em que foram informados:");
		alunos.forEach(System.out::println);

		System.out.println("\nNomes na ordem em \"decrescente de data de nascimento\":");
		// alunos.sort(Comparator.reverseOrder());
		alunos.sort(Comparator.comparing(Aluno::getDataNascimento).reversed());
		alunos.forEach(System.out::println);

		System.out.println("\nAlunos em ordem \"crescente de data de nascimento\":");
		// alunos.sort(Comparator.naturalOrder());
		alunos.sort(Comparator.comparing(Aluno::getDataNascimento));
		alunos.forEach(System.out::println);

		System.out.println("\nNomes na ordem em \"decrescente de matrícula\":");
		alunos.sort(Comparator.comparing(Aluno::getMatricula).reversed());
		alunos.forEach(System.out::println);

		System.out.println("\nAlunos em ordem \"crescente de matrícula\":");
		alunos.sort(Comparator.comparing(Aluno::getMatricula));
		alunos.forEach(System.out::println);

	}

	private static void ordenacaoJava7() {

		System.out.println("\n\n\n######## ordenacaoJava7 ########\n");

		List<Aluno> alunos = new ArrayList<>();

		alunos.add(new Aluno(200, "123", "claudio", LocalDate.of(1975, 5, 1)));
		alunos.add(new Aluno(190, "321", "pedreira", LocalDate.of(2000, 6, 20)));
		alunos.add(new Aluno(180, "345", "antonio", LocalDate.of(2000, 6, 20)));
		alunos.add(new Aluno(175, "154", "neiva", LocalDate.of(1990, 7, 5)));

		System.out.println("Alunos na ordem em que foram informados:");
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

		System.out.println("\nNomes na ordem em \"decrescente de data de nascimento\":");
		Collections.sort(alunos);
		Collections.reverse(alunos);
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

		System.out.println("\nAlunos em ordem \"crescente de data de nascimento\":");
		Collections.sort(alunos);
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

		System.out.println("\nNomes na ordem em \"decrescente de matrícula\":");
		// Geralmente não fazemos ordenação dessa forma:
		// Collections.sort(alunos, new AlunoMatriculaComparator());

		Collections.sort(alunos, new Comparator<>() {
			@Override
			public int compare(Aluno arg0, Aluno arg1) {
				return arg0.getMatricula().compareTo(arg1.getMatricula());
			}
		});
		Collections.reverse(alunos);
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

		System.out.println("\nAlunos em ordem \"crescente de matrícula\":");
		Collections.sort(alunos, new Comparator<>() {
			@Override
			public int compare(Aluno arg0, Aluno arg1) {
				return arg0.getMatricula().compareTo(arg1.getMatricula());
			}
		});
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

		System.out.println("\nAlunos em ordem \"crescente de ano de nascimento e decrescente de nome\":");
		Collections.sort(alunos, new Comparator<>() {
			@Override
			public int compare(Aluno arg0, Aluno arg1) {
				int resultado = arg0.getDataNascimento().getYear() - arg1.getDataNascimento().getYear();
				if (resultado == 0) {
					resultado = -1 * arg0.getNome().compareTo(arg1.getNome());
				}
				return resultado;
			}
		});
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

	}

}
