package br.ucsal.bes.poo20212.aula17.conjuntos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ConjuntosExemplo1 {

	private static final int QTD_NOMES = 5;

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		// Programa java que receba 5 nomes distintos. Enquanto não foram fornecidos os
		// 5 nomes, seu programa deve continuar solicitando que o usuário informe um
		// nome. Após a entrada dos 5 nomes, você deve listá-los.

		// obterExibirNomesDistintosList(QTD_NOMES);
		obterExibirNomesDistintosSet(QTD_NOMES);

	}

	private static void obterExibirNomesDistintosList(int qtdNomes) {
		List<String> nomes = new ArrayList<>();
		System.out.println("Informe " + qtdNomes + " nomes distintos:");
		do {
			String nome = scanner.nextLine();
			if (!nomes.contains(nome)) {
				nomes.add(nome);
			}
		} while (nomes.size() < qtdNomes);
		System.out.println("\nNomes informados:");
		nomes.forEach(System.out::println);
	}

	private static void obterExibirNomesDistintosSet(int qtdNomes) {
		Set<String> nomes = new HashSet<>();
		// Set<String> nomes = new LinkedHashSet<>();
		// Set<String> nomes = new TreeSet<>();
		System.out.println("Informe " + qtdNomes + " nomes distintos:");
		do {
			String nome = scanner.nextLine();
			nomes.add(nome);
		} while (nomes.size() < qtdNomes);
		System.out.println("\nNomes informados:");
		nomes.forEach(System.out::println);
	}

}
