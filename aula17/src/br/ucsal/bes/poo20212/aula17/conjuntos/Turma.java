package br.ucsal.bes.poo20212.aula17.conjuntos;

import java.util.Set;

import br.ucsal.bes.poo20212.aula17.ordenacao.Aluno;

public class Turma {

	private Integer codigo;

	private Set<Aluno> alunos;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Set<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(Set<Aluno> alunos) {
		this.alunos = alunos;
	}

}
