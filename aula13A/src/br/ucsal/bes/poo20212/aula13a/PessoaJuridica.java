package br.ucsal.bes.poo20212.aula13a;

import java.util.List;

public class PessoaJuridica extends Pessoa {

	private String cnpj;

	private String inscricaoEstadual;

	private String inscricaoMunicipal;

	public PessoaJuridica(String nome, String endereco, List<String> telefones, String cnpj, String inscricaoEstadual,
			String inscricaoMunicipal) {
		super(nome, endereco, telefones);
		this.cnpj = cnpj;
		this.inscricaoEstadual = inscricaoEstadual;
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	@Override
	public String obterIdentificador() {
		return cnpj;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	@Override
	public String toString() {
		return "PessoaJuridica [cnpj=" + cnpj + ", inscricaoEstadual=" + inscricaoEstadual + ", inscricaoMunicipal="
				+ inscricaoMunicipal + "]";
	}

}
