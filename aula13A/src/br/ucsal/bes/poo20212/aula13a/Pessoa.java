package br.ucsal.bes.poo20212.aula13a;

import java.util.List;

public abstract class Pessoa {

	private String nome;

	private String endereco;

	private List<String> telefones;

	public Pessoa(String nome, String endereco, List<String> telefones) {
		super();
		this.nome = nome;
		this.endereco = endereco;
		this.telefones = telefones;
	}

	public abstract String obterIdentificador();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", endereco=" + endereco + ", telefones=" + telefones + "]";
	}

}
