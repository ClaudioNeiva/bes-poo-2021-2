package br.ucsal.bes.poo20212.aula13a;

import java.time.LocalDate;
import java.util.List;

public class Enfermeiro extends PessoaFisica {

	private Integer coren;

	public Enfermeiro(String nome, String endereco, List<String> telefones, String cpf, String nomeMae,
			LocalDate dataNascimento, Integer coren) {
		super(nome, endereco, telefones, cpf, nomeMae, dataNascimento);
		this.coren = coren;
	}
	
	@Override
	public String obterIdentificador() {
		return coren.toString();
	}

	public Integer getCoren() {
		return coren;
	}

	public void setCoren(Integer coren) {
		this.coren = coren;
	}

	@Override
	public String toString() {
		return "Enfermeira [coren=" + coren + "]";
	}

}
