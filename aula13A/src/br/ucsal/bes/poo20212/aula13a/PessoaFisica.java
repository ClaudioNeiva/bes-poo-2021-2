package br.ucsal.bes.poo20212.aula13a;

import java.time.LocalDate;
import java.util.List;

public class PessoaFisica extends Pessoa {

	private String cpf;

	private String nomeMae;

	private LocalDate dataNascimento;

	public PessoaFisica(String nome, String endereco, List<String> telefones, String cpf, String nomeMae,
			LocalDate dataNascimento) {
		super(nome, endereco, telefones);
		this.cpf = cpf;
		this.nomeMae = nomeMae;
		this.dataNascimento = dataNascimento;
	}

	@Override
	public String obterIdentificador() {
		return cpf;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@Override
	public String toString() {
		return "PessoaFisica [cpf=" + cpf + ", nomeMae=" + nomeMae + ", dataNascimento=" + dataNascimento + "]";
	}

}
