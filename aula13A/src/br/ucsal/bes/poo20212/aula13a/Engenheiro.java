package br.ucsal.bes.poo20212.aula13a;

import java.time.LocalDate;
import java.util.List;

public class Engenheiro extends PessoaFisica {

	private Integer crea;

	public Engenheiro(String nome, String endereco, List<String> telefones, String cpf, String nomeMae,
			LocalDate dataNascimento, Integer crea) {
		super(nome, endereco, telefones, cpf, nomeMae, dataNascimento);
		this.crea = crea;
	}

	@Override
	public String obterIdentificador() {
		return crea.toString();
	}
	
	public Integer getCrea() {
		return crea;
	}

	public void setCrea(Integer crea) {
		this.crea = crea;
	}

	@Override
	public String toString() {
		return "Engenheiro [crea=" + crea + "]";
	}

}
