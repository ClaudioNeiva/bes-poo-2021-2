package br.ucsal.bes.poo20212.aula13a;

public class TuiUtil {

	public static void apresentarPessoa(Pessoa pessoa) {
		// System.out.println(pessoa.getNome() + " classe = " + pessoa.getClass());

		// Colocando ou não a chamada ao toString(), a mesma vai ocorrer no println
		// Late binding = só durante a execução, será possível saber qual corpo de
		// método será chamado, a depender da instância que estará sendo apontada pela
		// variável pessoa durante a execução do método!
		// System.out.println(pessoa.toString());

		// NÃO DEVEMOS FAZER ASSIM: MUITO TRABALHO, MUITO RISCO DE CODIFICAR ERRADO E
		// FALTA DE CONHECIMENTO SOBRE POLIMORFISMO!
		// System.out.print("Identificador: ");
		// if (pessoa instanceof Medico) {
		// Medico medico = (Medico) pessoa;
		// System.out.println(medico.getNumCRM() + " " + medico.getUfCRM());
		// } else if (pessoa instanceof PessoaFisica) {
		// System.out.println(((PessoaFisica) pessoa).getCpf());
		// } else if (pessoa instanceof PessoaJuridica) {
		// System.out.println(((PessoaJuridica) pessoa).getCnpj());
		// }

		System.out.println("Identificador: " + pessoa.obterIdentificador());
		System.out.println("Nome: " + pessoa.getNome());
		System.out.println("Endereço: " + pessoa.getEndereco());
		System.out.println();
	}

}
