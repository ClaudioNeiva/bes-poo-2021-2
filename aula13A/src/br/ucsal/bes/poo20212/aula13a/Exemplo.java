package br.ucsal.bes.poo20212.aula13a;

import java.time.LocalDate;
import java.util.Arrays;

public class Exemplo {

	public static void main(String[] args) {

		PessoaFisica pessoaFisica1 = new PessoaFisica("Claudio", "Rua x", Arrays.asList("71234543543", "714356456"),
				"13123123", "Maria", LocalDate.of(1980, 10, 5));

		PessoaJuridica pessoaJuridica1 = new PessoaJuridica("UCSal", "Pinto de Aguiar",
				Arrays.asList("714356256", "7123213234"), "01223435000123", "4353645645", "342345");

		Medico medico1 = new Medico("Ana", "Rua Y", Arrays.asList("71345634534"), "7547457", "Clara",
				LocalDate.of(1990, 5, 9), 12345, "BA");

		Engenheiro engenheiro1 = new Engenheiro("Clara", "Rua Z", Arrays.asList("12312312"), "8678787", "Lais",
				LocalDate.of(2000, 10, 10), 87878);

		Enfermeiro enfermeiro = new Enfermeiro("Iago", "Ruz K", Arrays.asList("1231"), "54675765", "Joana",
				LocalDate.of(1999, 5, 3), 776655);

		TuiUtil.apresentarPessoa(pessoaFisica1);

		TuiUtil.apresentarPessoa(pessoaJuridica1);

		TuiUtil.apresentarPessoa(medico1);

		TuiUtil.apresentarPessoa(engenheiro1);

		TuiUtil.apresentarPessoa(enfermeiro);

	}

}
