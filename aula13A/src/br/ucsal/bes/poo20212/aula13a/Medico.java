package br.ucsal.bes.poo20212.aula13a;

import java.time.LocalDate;
import java.util.List;

public class Medico extends PessoaFisica {

	private Integer numCRM;

	private String ufCRM;

	public Medico(String nome, String endereco, List<String> telefones, String cpf, String nomeMae,
			LocalDate dataNascimento, Integer numCRM, String ufCRM) {
		super(nome, endereco, telefones, cpf, nomeMae, dataNascimento);
		this.numCRM = numCRM;
		this.ufCRM = ufCRM;
	}

	@Override
	public String obterIdentificador() {
		return numCRM + " " + ufCRM;
	}

	public Integer getNumCRM() {
		return numCRM;
	}

	public void setNumCRM(Integer numCRM) {
		this.numCRM = numCRM;
	}

	public String getUfCRM() {
		return ufCRM;
	}

	public void setUfCRM(String ufCRM) {
		this.ufCRM = ufCRM;
	}

	@Override
	public String toString() {
		return "Medico [numCRM=" + numCRM + ", ufCRM=" + ufCRM + "]";
	}

}
