package br.ucsal.bes.poo20212.aula18.conjuntos;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import br.ucsal.bes.poo20212.aula18.mapas.Aluno;

public class HashCodeExemplo {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno(200, "123", "claudio", LocalDate.of(1975, 5, 1));
		Aluno aluno2 = new Aluno(180, "345", "antonio", LocalDate.of(2000, 6, 20));
		Aluno aluno3 = new Aluno(456, "129", "maria", LocalDate.of(2005, 7, 28));
		Aluno aluno4 = new Aluno(129, "789", "ana", LocalDate.of(2000, 9, 27));
		Aluno aluno5 = new Aluno(567, "912", "pedro", LocalDate.of(1990, 8, 28));

		System.out.println("aluno1.hashCode()=" + aluno1.hashCode());
		System.out.println("aluno2.hashCode()=" + aluno2.hashCode());
		System.out.println("aluno3.hashCode()=" + aluno3.hashCode());
		System.out.println("aluno5.hashCode()=" + aluno5.hashCode());

		Set<Aluno> alunos = new HashSet<>();
		alunos.add(aluno1);
		alunos.add(aluno2);
		alunos.add(aluno3);
		alunos.add(aluno4);
		alunos.add(aluno5);

		System.out.println("\n");
		System.out.println("alunos.contains(aluno1)=" + alunos.contains(aluno1));
		System.out.println("alunos.contains(aluno5)=" + alunos.contains(aluno5));

		System.out.println("\n");
		aluno5.setNome("joão");
		System.out.println("aluno5[com nome joão].hashCode()=" + aluno5.hashCode());
		System.out.println("alunos.contains(aluno5)=" + alunos.contains(aluno5));
		System.out.println("alunos.remove(aluno5)=" + alunos.remove(aluno5));
		alunos.forEach(System.out::println);

		System.out.println("\n");
		aluno5.setNome("pedro");
		System.out.println("aluno5[com nome pedro].hashCode()=" + aluno5.hashCode());
		System.out.println("alunos.contains(aluno5)=" + alunos.contains(aluno5));
		System.out.println("alunos.remove(aluno5)=" + alunos.remove(aluno5));
		alunos.forEach(System.out::println);

	}
}
