package br.ucsal.bes.poo20212.aula18.mapas;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

public class MapasExemplo1 {

	private static final int QTD_NUMEROS = 5;

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		/*
		 * Crie um programa java que solicite 5 números para o usuário. Após a entrada
		 * dos 5 numéros, seu programa deve listar os distintos números e quantidade de
		 * vezes que o número foi informado.
		 * 
		 * Exemplo1:
		 * 
		 * Entrada: 4, 8, 3, 3 e 8
		 * 
		 * Saída: 4 x 1, 8 x 2, 3 x 2
		 * 
		 * Exemplo2:
		 * 
		 * Entrada: 4, 4, 9, 9 e 4
		 * 
		 * Saída: 4 x 3, 9 x 2
		 * 
		 */

//		obterExibirNumerosDistintosQtdListas(QTD_NUMEROS);
//		obterExibirNumerosDistintosQtdArranjos(QTD_NUMEROS);
		obterExibirNumerosDistintosQtdMapas(QTD_NUMEROS);
	}

	private static void obterExibirNumerosDistintosQtdMapas(int qtdNumeros) {
		Map<Integer, Integer> numerosDistintosQtds = new HashMap<>();

		System.out.println("Informe " + qtdNumeros + " números:");
		for (int i = 0; i < qtdNumeros; i++) {
			
			Integer n = scanner.nextInt();
			
			numerosDistintosQtds.putIfAbsent(n, 0);
			numerosDistintosQtds.put(n, numerosDistintosQtds.get(n) + 1);
		}

		System.out.println("\n\nNúmeros distintos x qtd repetições:");
		for(Entry<Integer, Integer> numeroDistintoQtd : numerosDistintosQtds.entrySet()) {
			System.out.println(numeroDistintoQtd.getKey() + " x " + numeroDistintoQtd.getValue());
		}
	}

	private static void obterExibirNumerosDistintosQtdListas(int qtdNumeros) {
		List<Integer> numerosDistintos = new ArrayList<>();
		List<Integer> qtdRepeticoes = new ArrayList<>();

		System.out.println("Informe " + qtdNumeros + " números:");
		for (int i = 0; i < qtdNumeros; i++) {

			Integer n = scanner.nextInt();

			if (numerosDistintos.contains(n)) {
				// Incrementar a quantidade de repeticoes.
				int pos = numerosDistintos.indexOf(n);
				int qtdRepeticoesN = qtdRepeticoes.get(pos);
				qtdRepeticoes.set(pos, qtdRepeticoesN + 1);
			} else {
				// Incluir o número no numerosDistintos e definir a qtd de repetições como 1.
				numerosDistintos.add(n);
				qtdRepeticoes.add(1);
			}

		}

		System.out.println("\n\nNúmeros distintos x qtd repetições:");
		for (int i = 0; i < numerosDistintos.size(); i++) {
			System.out.println(numerosDistintos.get(i) + " x " + qtdRepeticoes.get(i));
		}

	}

	private static void obterExibirNumerosDistintosQtdMapasObservacoes(int qtdNumeros) {
		// Map <key = numero-distinto , value = quantidade-repeticoes>
		Map<Integer, Integer> numerosDistintosQtds = new HashMap<>();

		System.out.println("Informe " + qtdNumeros + " números:");
		for (int i = 0; i < qtdNumeros; i++) {
			Integer n = scanner.nextInt();
			
			numerosDistintosQtds.putIfAbsent(n, 0);
			numerosDistintosQtds.put(n, numerosDistintosQtds.get(n) + 1);
			
//			if (numerosDistintosQtds.containsKey(n)) {
//				int qtdRepeticoesN = numerosDistintosQtds.get(n);
//				numerosDistintosQtds.put(n, qtdRepeticoesN + 1);
//			} else {
//				numerosDistintosQtds.put(n, 1);
//			}
		}

		System.out.println("\n\nNúmeros distintos x qtd repetições:");
		for(Entry<Integer, Integer> numeroDistintoQtd : numerosDistintosQtds.entrySet()) {
			System.out.println(numeroDistintoQtd.getKey() + " x " + numeroDistintoQtd.getValue());
		}
		
		Set<Integer> keys = numerosDistintosQtds.keySet(); // retorna as chaves
		Collection<Integer> values = numerosDistintosQtds.values(); // retorna o conjunto de valores
	}
	
	
}
