package br.ucsal.bes.poo20212.aula13b.exemplo;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {

		LinkedList<String> nomesLinkedList = new LinkedList<>();
		
		List<String> nomesArrayList = new ArrayList<>();

		adicionarNome("claudio", nomesLinkedList);
		adicionarNome("neiva", nomesArrayList);

		adicionarPrimeiroNomeFila("claudio", nomesLinkedList);
		
	}

	public static void adicionarNome(String nome, List<String> lista) {
		lista.add(nome);
	}

	public static void adicionarPrimeiroNomeFila(String nome, Deque<String> fila) {
		fila.addFirst(nome);
	}

}
