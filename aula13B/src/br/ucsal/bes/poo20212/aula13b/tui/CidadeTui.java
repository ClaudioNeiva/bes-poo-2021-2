package br.ucsal.bes.poo20212.aula13b.tui;

import br.ucsal.bes.poo20212.aula13b.domain.Cidade;
import br.ucsal.bes.poo20212.tuitools.ComponenteTui;

public class CidadeTui extends Cidade implements ComponenteTui {

	public CidadeTui(String sigla, String nome) {
		super(sigla, nome);
	}

	@Override
	public void apresentar() {
		System.out.println("Sigla = "+getSigla());
		System.out.println("Nome = "+getNome());
	}

	@Override
	public void outroMetodo() {
		System.out.println("Fazer alguma coisa");
	}

}
