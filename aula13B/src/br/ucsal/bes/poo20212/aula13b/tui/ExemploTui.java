package br.ucsal.bes.poo20212.aula13b.tui;

import br.ucsal.bes.poo20212.tuitools.TuiUtil;

public class ExemploTui {

	public static void main(String[] args) {
		
		CidadeTui cidadeTui1 = new CidadeTui("SSA", "Salvador");
		
		TuiUtil.apresentarJanela("Cidade1", cidadeTui1);
		
	}
	
}
