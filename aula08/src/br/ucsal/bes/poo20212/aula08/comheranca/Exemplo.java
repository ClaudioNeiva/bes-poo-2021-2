package br.ucsal.bes.poo20212.aula08.comheranca;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {

		PessoaFisica pessoaFisica = new PessoaFisica();
		pessoaFisica.cpf = "98769879789";
		pessoaFisica.nome = "Claudio Neiva";
		pessoaFisica.endereco = "Rua X";
		pessoaFisica.nomeMae = "Maria da Silva";
		pessoaFisica.dataNascimento = LocalDate.of(2000, 5, 15);
		pessoaFisica.telefones = Arrays.asList("71843843");

		PessoaJuridica pessoaJuridica = new PessoaJuridica();
		pessoaJuridica.cnpj = "156123123123000123";
		pessoaJuridica.nome = "Loja ABC";
		pessoaJuridica.endereco = "Rua Y";
		pessoaJuridica.inscricaoEstadual = "234";
		pessoaJuridica.inscricaoMunicipal = "8934765";
		pessoaJuridica.telefones = Arrays.asList("7123123123", "716723872", "4712342345");

		Medico medico = new Medico();
		medico.numCRM = 123;
		medico.ufCRM = "BA";
		medico.cpf = "12313123123";
		medico.nomeMae = "Maria da Silva";
		medico.dataNascimento = LocalDate.of(1910, 1, 1);
		medico.nome = "João da Silva";
		medico.endereco = "Rua W";
		medico.telefones = Arrays.asList("7123123123", "71998989898");

		// Não é possível instanciar Pessoa, pois ela tem o modificador "abstract" na
		// sua definição.
		// Pessoa pessoa = new Pessoa();
		// pessoa.nome = "Alguma coisa";
		// pessoa.endereco = "Alguma rua";

		List<Pessoa> clientes = new ArrayList<>();
		clientes.add(pessoaFisica); // UP CAST
		clientes.add(pessoaJuridica); // UP CAST
		clientes.add(medico); // UP CAST
		// clientes.add(123); //SÓ serão aceitas instâncias de Pessoa e suas subclasses.

		System.out.println("Nomes dos clientes:");
		for (Pessoa cliente : clientes) {
			apresentarCliente(cliente);
		}

		System.out.println("***************************");
		apresentarCliente2(pessoaFisica);
		apresentarCliente2(pessoaJuridica);
		apresentarCliente2(medico);
	}

	private static void apresentarCliente2(Pessoa pessoa) {
		System.out.println("\nnome=" + pessoa.nome);
		System.out.println("endereco=" + pessoa.endereco);
		System.out.println("telefones=" + pessoa.telefones);
	}

	private static void apresentarCliente(Pessoa cliente) {
		System.out.print("nome=" + cliente.nome);
		if (cliente instanceof PessoaFisica) {
			// PessoaFisica pessoaFisica1 = (PessoaFisica) cliente;
			// System.out.println(" - cpf=" + pessoaFisica1.cpf);
			System.out.println(" - cpf=" + ((PessoaFisica) cliente).cpf);
		} else if (cliente instanceof PessoaJuridica) {
			System.out.println(" - cnpj=" + ((PessoaJuridica) cliente).cnpj);
		}
		if (cliente instanceof Medico) {
			System.out.println(" - crm=" + ((Medico) cliente).numCRM);
		}
	}

}
