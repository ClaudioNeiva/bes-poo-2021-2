package br.ucsal.bes.poo20212.aula08.comheranca;

import java.time.LocalDate;
import java.util.Arrays;

public class Exemplo2 {

	public static void main(String[] args) {

		// up cast
		Pessoa pessoa1 = new PessoaFisica();
		pessoa1.nome = "Joaquim";
		pessoa1.endereco = "Rua x";
		pessoa1.telefones = Arrays.asList("711213123");
		// pessoa1.cpf = "123123123";

		// down cast
		PessoaFisica pessoaFisica1 = (PessoaFisica) pessoa1;
		pessoaFisica1.cpf = "123123123";
		pessoaFisica1.nomeMae = "Maria";
		pessoaFisica1.dataNascimento = LocalDate.of(2000, 5, 8);

		System.out.println("pessoa1       = " + pessoa1);
		System.out.println("pessoaFisica1 = " + pessoaFisica1);

	}

}
