package br.ucsal.bes.poo20212.aula08.comheranca;

import java.util.List;

// Mesmo que você NÃO indique que pessoa é subclasse de Object, ela SERÁ.
// NORMALMENTE nós NÃO indicamos o extends Object, por ser o default (implícito).
// public class Pessoa extends Object {

// A classe pessoa é modificada para "abstract" para indicar que ela é um conceito, 
// mas não deve ser possível criar instâncias da mesma.
public abstract class Pessoa {

	String nome;

	String endereco;

	List<String> telefones;

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", endereco=" + endereco + ", telefones=" + telefones + "]";
	}

}
