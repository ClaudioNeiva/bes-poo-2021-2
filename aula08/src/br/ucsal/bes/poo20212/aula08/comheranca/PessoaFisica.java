package br.ucsal.bes.poo20212.aula08.comheranca;

import java.time.LocalDate;

public class PessoaFisica extends Pessoa {

	String cpf;

	String nomeMae;

	LocalDate dataNascimento;

	@Override
	public String toString() {
		return "PessoaFisica [cpf=" + cpf + ", nomeMae=" + nomeMae + ", dataNascimento=" + dataNascimento
				+ ", toString()=" + super.toString() + "]";
	}

}
