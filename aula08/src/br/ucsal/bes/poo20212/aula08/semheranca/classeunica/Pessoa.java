package br.ucsal.bes.poo20212.aula08.semheranca.classeunica;

import java.time.LocalDate;
import java.util.List;

public class Pessoa {

	String cpf;

	String nome;

	String endereco;

	String nomeMae;

	LocalDate dataNascimento;

	String cnpj;

	String inscricaoEstadual;

	String inscricaoMunicipal;

	List<String> telefones;

}
