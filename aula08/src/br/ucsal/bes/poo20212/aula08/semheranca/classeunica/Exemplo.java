package br.ucsal.bes.poo20212.aula08.semheranca.classeunica;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {

		Pessoa pessoaFisica = new Pessoa();
		pessoaFisica.cpf = "98769879789";
		pessoaFisica.nome = "Claudio Neiva";
		pessoaFisica.endereco = "Rua x";
		pessoaFisica.nomeMae = "Maria da Silva";
		pessoaFisica.dataNascimento = LocalDate.of(2000, 5, 15);

		Pessoa pessoaJuridica = new Pessoa();
		pessoaJuridica.cnpj = "156123123123000123";
		pessoaJuridica.nome = "Loja ABC";
		pessoaJuridica.endereco = "Rua y";
		pessoaJuridica.inscricaoEstadual = "234";
		pessoaJuridica.inscricaoMunicipal = "8934765";

		// NÃO deveria ser possível instanciar pessoas com características de pessoas
		// físicas e jurídicas na mesma instância. Para este requisito.
		Pessoa pessoaHibrida = new Pessoa();
		pessoaHibrida.cnpj = "23434344344000125";
		pessoaHibrida.nome = "Loja XYZ";
		pessoaHibrida.nomeMae = "Joana da Silva";
		pessoaHibrida.endereco = "Rua y";
		pessoaHibrida.inscricaoEstadual = "234";
		pessoaHibrida.inscricaoMunicipal = "8934765";
		pessoaHibrida.dataNascimento = LocalDate.of(2010, 5, 15);

		List<Pessoa> clientes = new ArrayList<>();
		clientes.add(pessoaFisica);
		clientes.add(pessoaJuridica);
		// clientes.add(21341234); // NÃO serão aceitas instâncias que não sejam de
		// Pessoa.

	}

}
