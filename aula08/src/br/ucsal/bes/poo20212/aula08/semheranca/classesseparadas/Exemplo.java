package br.ucsal.bes.poo20212.aula08.semheranca.classesseparadas;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {

		PessoaFisica pessoaFisica = new PessoaFisica();
		pessoaFisica.cpf = "98769879789";
		pessoaFisica.nome = "Claudio Neiva";
		pessoaFisica.endereco = new Endereco();
		pessoaFisica.nomeMae = "Maria da Silva";
		pessoaFisica.dataNascimento = LocalDate.of(2000, 5, 15);

		PessoaJuridica pessoaJuridica = new PessoaJuridica();
		pessoaJuridica.cnpj = "156123123123000123";
		pessoaJuridica.nome = "Loja ABC";
		pessoaJuridica.endereco = new Endereco();
		pessoaJuridica.inscricaoEstadual = "234";
		pessoaJuridica.inscricaoMunicipal = "8934765";

		// NÃO é possível criar uma lista de clientes que contenha instâncias de pessoas
		// físicas e pessoas jurídicas, na mesma lista, mas não permita outros tipo de
		// objeto na mesma.
		// Essa lista aceita QUALQUER tipo de objeto, o que não é desejável para a
		// estrutura de uma de clientes.
		List<Object> clientes = new ArrayList<>();
		clientes.add(pessoaFisica);
		clientes.add(pessoaJuridica);
		clientes.add(12331231); // 12331231 é um cliente?????
		clientes.add(LocalDate.of(2000, 2, 4)); // 04/02/2000 é um cliente?????

	}

}
