package br.ucsal.bes.poo20212.aula08.semheranca.classesseparadas;

import java.time.LocalDate;
import java.util.List;

public class PessoaFisica {

	String cpf;

	String nome;

	Endereco endereco;

	String nomeMae;

	LocalDate dataNascimento;
	
	List<String> telefones;

}
